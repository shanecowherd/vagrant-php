group { "puppet":
  ensure => "present",
}

class apache { 
  package { "apache2": ensure => present } 
  service { "apache2": ensure => running } 
} 

class apache::disabled inherits apache { 
  Package["apache2"] { ensure => absent } 
  Service["apache2"] { ensure => stopped } 
}       

class apache::uninstall {
  package { 'apache2': ensure => purged }
  package { 'apache2-utils': ensure => purged }
  package { 'apache2.2-bin': ensure => purged }
  package { 'apache2.2-common': ensure => purged }

}

file {
  "httpd.conf":
    notify  => Service["apache2"],
    mode => 644,
    owner => root,
    group => root,
    path => "/etc/apache2/httpd.conf",
    source => "/vagrant/httpd.conf",
}

class php {
  package { "php5": ensure => present }
  package { "libapache2-mod-php5": ensure => present }
  package { "php5-mysql": ensure => present }
}


class { 'mysql::server':
  config_hash => { 'root_password' => 'vagrant', 'bind_address'  => $::ipaddress }
}


mysql::db { 'development':
  user     => 'vagrant',
  password => 'vagrant',
  host     => 'localhost',
  grant    => ['all'],
}

database_grant { 'development@%/development':
  privileges => ['all'] ,
}

define apache::loadmodule () {
  exec { "/usr/sbin/a2enmod $name" :
    unless => "/bin/readlink -e /etc/apache2/mods-enabled/${name}.load",
           notify => Service[apache2]
  }
}

apache::loadmodule{"rewrite": }

include apache
include mysql
include php


